# ListView Example
https://img.shields.io/badge/cython-0.29-blue
[![Python 3.x Support](https://img.shields.io/pypi/pyversions/Django.svg)](https://python.org)
[![Cython 0.29 Support](https://img.shields.io/badge/cython-0.29-blue.svg)](https://python.org)
[![License: AGPL v3+](https://img.shields.io/badge/license-AGPL%20v3%2B-blue.svg)](http://www.gnu.org/licenses/agpl-3.0) 

This example shows how to use mainly `ui` XML files to setup listviews in GTK 4.

Also, I think this is the only existing PyGobject Cython example.

![](data/screenshot.png)
