from functools import partial
from gi.repository import GObject, Gtk
from pprint import pprint

class CustomListItem(Gtk.ListItem):
    __gtype_name__ = 'CustomListItem'
    __gsignals__ = {'selected':(sf.RUN_LAST,
                                TYPE_NONE,
                                (TYPE_PYOBJECT,))}

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

