# list_item_box.py
#
# Copyright 2021 Pellegrino Prevete
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GObject, Gtk
from gi.repository.GObject import SignalFlags as sf
from gi.repository.GObject import TYPE_NONE, TYPE_STRING, TYPE_PYOBJECT

@Gtk.Template(resource_path='/org/gnome/gitlab/tallero/listviewexample/list_item_box/list_item_box.ui')
class ListItemBox(Gtk.Box):
    __gtype_name__ = 'ListItemBox'
    __gsignals__ = {'label-pressed':(sf.RUN_LAST,
                                     TYPE_NONE,
                                     (TYPE_PYOBJECT,))}

    label_obj = Gtk.Template.Child()
    description_obj = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @GObject.Property(type=str)
    def label(self):
        return self.label_obj.get_label()

    @label.setter
    def label(self, value):
        self.label_obj.set_label(value)

    @GObject.Property(type=str)
    def description(self):
        return self.description_obj.get_label()

    @description.setter
    def description(self, value):
        self.description_obj.set_label(value)

    @Gtk.Template.Callback()
    def pressed_cb(self, *args):
        self.emit("label-pressed", self.label_obj.get_label())
