# list_item_simple_factory_scope.py
#
# Copyright 2021 Pellegrino Prevete
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from functools import partial
from gi.repository import GObject, Gtk

# ListView signal
@GObject.Signal(flags=GObject.SignalFlags.RUN_LAST,
                return_type=bool,
                arg_types=(object,),
                accumulator=GObject.signal_accumulator_true_handled)
def label_pressed_cb(listitem, label, *args):
    print(f'listitem has emitted signal label_pressed')

class ListItemSimpleScope(GObject.GObject, Gtk.BuilderScope):
    __gtype_name__ = 'ListItemSimpleScope'

    def __init__(self):
        super().__init__()

    def do_create_closure(self, builder, func_name, flags, obj):
        return partial(globals()[func_name])
