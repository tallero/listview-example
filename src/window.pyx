# window.py
#
# Copyright 2021 Pellegrino Prevete
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gio, Gtk
from gi.repository import Adw

from gi.repository.Adw import Leaflet

from .list_item_box.list_item_box_factory_scope import ListItemBoxScope
from .list_item_box.list_item_box import ListItemBox

from .list_item_simple.list_item_simple_factory_scope import ListItemSimpleScope
from .list_item_simple.list_item_simple import ListItemSimple

@Gtk.Template(resource_path='/org/gnome/gitlab/tallero/listviewexample/window.ui')
class ListViewExampleWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'ListViewExampleWindow'

    first_liststore = Gtk.Template.Child()
    second_liststore = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        N = 1000

        gstr = lambda x: Gtk.StringObject.new(str(x))

        first_list = [('this', 'subject'),
                      ('is', 'verb'),
                      ('a', 'article'),
                      ('nice', 'adjective'),
                      ('short', 'adjective'),
                      ('list', 'name'),
                      ('store', 'name')]

        second_list = [gstr(f'item {x}') for x in range(N)]

        # Slow add
        for item in first_list:
            item_liststore = Gio.ListStore()
            for entry in item:
                item_liststore.append(gstr(entry))
            self.first_liststore.append(item_liststore)

        # Fast batch add
        self.second_liststore.splice(0, 0, second_list)

    @Gtk.Template.Callback()
    def selected_cb(self, listview, position):
        print(f'window: row {position} selected')
